/*

Start several clients through the Windows command line:

SET GRIDGAIN_HOME=[#PATH#]\gridgain-fabric-os-6.6.2
SET JAVA_HOME=[#PATH#]\jdk1.8.0_31
cd [#PATH#]\gridgain-fabric-os-6.6.2\bin
ggstart.bat examples/config/example-compute.xml


"Hello World" should show up on all clients.

*/


package example_hello_world;

import org.gridgain.grid.Grid;
import org.gridgain.grid.GridException;
import org.gridgain.grid.GridGain;
import org.gridgain.grid.lang.GridRunnable;

public class Main {

	public static void main(String[] args) throws Exception {
		try(Grid grid = GridGain.start("examples/config/example-compute.xml")) {
		//try(Grid grid = GridGain.start("config/default-config.xml")) {
			grid.compute().broadcast(new GridRunnable() {
				@Override
				public void run() {
					System.out.println("Hello World");
				}
			}).get();
		}

	}

}
